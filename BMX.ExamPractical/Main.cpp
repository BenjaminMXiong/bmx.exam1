// Ben Xiong Simple Calculator
#include <iostream>
#include <conio.h>
using namespace std;
//This helps create the equation so the user can type it in the console


float square(float a, float b)
{
	if (b == '2')
		return (pow(a, 2));
}
float cube(float a, float b)
{
	if (b == '3')
		return (pow(a, 3));
}


int main()
{
	int a;
	char b;
	char end = 'y';
	while (end != 'n')
	{
		cout << "Enter a number " << endl;
		cin >> a;

		cout << "Enter 2 to square or 3 to cube" << endl;
		cin >> b;



		if (b == '2')
		{
			cout << "Answer: " << square(a, b) << endl;
		}

		else if (b == '3')
		{
			cout << "Answer: " << cube(a, b) << endl;

		}

		else if (b < 2 || b > 3)
		{
			cout << "Please type either 2 to square or 3 to cube." << endl;
		}




		cout << "Will you like to continue? Enter y or n: ";
		cin >> end;
	}

	return 0;
	_getch();

}
